# GitLab CI script for Drupal 7 websites.
# @see https://www.dx-experts.nl/blog/2018/drupal-8-and-gitlab-ci-setup-guide
# @see https://gitlab.com/dx-experts/drupal-templates-for-gitlab-ci
#
# This script deploys a Drupal 7 site on each commit on the master-branch.
#
# Preparation:
# 1. Create ssh-key locally
#   $ ssh-keygen -t ed25519 -C "gitlabuser@example.com" -f ~/Downloads/id_ed25519_gitlab_example
# 2. Add ssh-key to authorized_keys on server
#   $ cat ~/Downloads/id_ed25519_gitlab_example.pub | ssh gitlabuser@example.com 'cat >> .ssh/authorized_keys'
# 3. Validate password-less login to server
#   $ ssh gitlabuser@example.com -i  ~/Downloads/id_ed25519_gitlab_example
# 4. Save $SSH_PRIVATE_KEY in GitLab CI variables.
#
# Debugging:
# - Double check if you can login, without password, with the public/private-key pair.
#   ssh gitlabuser@example.com -i gitlabuser-public-key.pub
# - When you login on the server yourself you must be able to run all commands in the deploy_prod script manually
#   without errors. Add –-verbose tags those commands that fail for further investigation.
# - When using Fish command-line replace && with ; to separate commands.
# - Use an absolute path to the Drush or Composer executables
#   DRUSH: "~/.composer/vendor/bin/drush"
#   COMPOSER: "php ~/.composer/composer.phar"
#   @see https://askubuntu.com/questions/810098/why-doesnt-my-alias-work-over-ssh
variables:
  GIT_ROOT: "/var/www/my-website/"
  SSH_LOGIN: "ssh -p22 -oStrictHostKeyChecking=no gitlabuser@example.com"
  DRUSH: "drush"
before_script:
# @see https://docs.gitlab.com/ee/ci/ssh_keys/
- 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
- eval $(ssh-agent -s)
- echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
- mkdir -p ~/.ssh
- chmod 700 ~/.ssh
deploy_master:
  when: always
  only:
  - master
  script:
  - $SSH_LOGIN "cd $GIT_ROOT && git fetch --tags"
  - $SSH_LOGIN "cd $GIT_ROOT && git pull"
  # Updates must be run before features are reverted.
  # https://drupal.stackexchange.com/questions/188840/what-order-should-configuration-import-and-module-updates-be-run
  - $SSH_LOGIN "cd $GIT_ROOT && $DRUSH updatedb -y"
  - $SSH_LOGIN "cd $GIT_ROOT && $DRUSH features-revert-all -y"
  # (Additional) sanitizing tasks.
  - $SSH_LOGIN "cd $GIT_ROOT && $DRUSH core-cron"
  # You should consider fine-grained cache invalidation
  # @see https://www.dx-experts.nl/blog/2017/drupal-8-development-caching
  - $SSH_LOGIN "cd $GIT_ROOT && $DRUSH cache-clear all -y"